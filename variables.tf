variable domain {}
variable unit {}
variable environment {}
variable project {}
variable k8s_namespace {}
variable service_name {}
variable service_port {
  type = number
}
variable service_target_ips {
  type = list(string)
}
variable service_target_ports {
  type = list(string)
}
