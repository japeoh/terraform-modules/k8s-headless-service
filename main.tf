resource kubernetes_endpoints service {
  metadata {
    name      = var.service_name
    namespace = var.k8s_namespace
    labels    = local.labels
  }

  dynamic subset {
    for_each = var.service_target_ips
    iterator = ip
    content {
      address {
        ip = ip.value
      }

      dynamic port {
        for_each = var.service_target_ports
        iterator = port
        content {
          port     = element(split("/", port.value), 0)
          protocol = element(split("/", port.value), 1)
        }
      }
    }
  }
}

resource kubernetes_service service {
  metadata {
    name      = kubernetes_endpoints.service.metadata.0.name
    namespace = var.k8s_namespace
    labels    = local.labels
  }

  spec {
    dynamic port {
      for_each = var.service_target_ports
      iterator = port
      content {
        port        = var.service_port
        target_port = element(split("/", port.value), 0)
      }
    }
  }
}
